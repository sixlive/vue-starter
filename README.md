Vue.js Starter using Laravel Elixir
=====================================

Usage
------
1. Install packages `npm install`
2. Compile Assets
    - Compile only: `gulp`
    - Watch / BroswerSync : `gulp watch`

Elixir Usage
-------------
https://laravel.com/docs/5.2/elixir

Bootstrap Usage
-----------------
To enable Bootstrap Scss: uncomment `@import "../../../node_modules/bootstrap-sass/assets/stylesheets/bootstrap.scss";` in `resources/sass/style.scss`.

To enable Bootstrap JS: uncomment `window.$ = window.jQuery = require('jquery');` and `var bootstrapjs = require('bootstrap-sass');` in `resources/js/main.js`
