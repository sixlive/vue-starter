const elixir = require('laravel-elixir')

elixir(function(mix) {
  mix.sass('style.scss')
  mix.browserify('main.js');
  mix.browserSync({
    proxy: null,
    server: './public'
  })
})
